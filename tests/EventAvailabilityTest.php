<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 13/02/2018
 * Time: 10:15
 */
namespace peraladaweb\koobinphp;

require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use Peralada\Koobin\Request\GateWay;
use Peralada\Koobin\Request\EventAvailabilityRequest;
use Peralada\Koobin\Response\EventAvailabilityResponse;

class EventAvailabilityTest extends TestCase
{
    /**
     * @dataProvider gateWayProvider
     */
    public function testRequestResponse($gateWay)
    {
        $eventAvailabilityResquest = new EventAvailabilityRequest($gateWay);
        $eventAvailabilityResquest->setEventId(1);
        $eventAvailabilityRespose = $eventAvailabilityResquest->request();

        var_dump($eventAvailabilityResquest);die;
        try {
            $this->assertInstanceOf(EventAvailabilityResponse::class, $eventAvailabilityRespose);
        } catch (Exception $e) {
            var_dump($e);die;
        }
    }

    /**
     * @dataProvider gateWayProvider
     */
    public function testResponse($gateWay)
    {
        $eventAvailabilityResquest = new EventAvailabilityRequest($gateWay);
        $eventAvailabilityResquest->setEventId(1);
        $eventAvailabilityRespose = $eventAvailabilityResquest->request();

        $this->assertInstanceOf(EventAvailabilityResponse::class, $eventAvailabilityRespose);
    }

    public function gateWayProvider()
    {
        $gateWay = new GateWay();

        return $gateWay;
    }
}