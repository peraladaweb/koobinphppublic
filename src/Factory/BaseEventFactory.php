<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 11:46
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\BaseEvent;

class BaseEventFactory
{
    public function createBaseEvent($XMLResponse)
    {
        $baseEvent = new BaseEvent();
        $baseEvent->setBaseEventId((int) $XMLResponse['base_event_id']);
        $baseEvent->setTitle((string) $XMLResponse['title']);
        $baseEvent->setOrganizerComanyId((int) $XMLResponse['organizer_company_id']);
        $baseEvent->setOrganizerCompanyVatNumber((string) $XMLResponse['organizer_company_vat_number']);

        $events = [];
        $eventFactory = new EventFactory();
        if (isset($XMLResponse->event)) {
            foreach ($XMLResponse->event as $item) {
                $events[] = $eventFactory->createEvent($item);
            }
        }

        $baseEvent->setEvents($events);

        return $baseEvent;
    }
}