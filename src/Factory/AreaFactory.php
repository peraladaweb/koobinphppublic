<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 12:54
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Area;

class AreaFactory
{
    public function createArea($XMLResponse)
    {
        $area = new Area();
        $area->setAreaId((int) $XMLResponse['area_id']);
        $area->setDescription((string) $XMLResponse['description']);

        $rates = [];
        $rateFactory = new RateFactory();

        if (isset($XMLResponse->rates) && isset($XMLResponse->rates->rate)) {
            foreach ($XMLResponse->rates->rate as $item) {
                $rates[] = $rateFactory->createRate($item);
            }
        }

        $area->setRates($rates);

        return $area;
    }
}