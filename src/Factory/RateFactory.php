<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 12:57
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Rate;

class RateFactory
{
    public function createRate($XMLResponse)
    {
        $rate = new Rate();
        $rate->setRateId((int) $XMLResponse['rate_id']);
        $rate->setDescription((string) $XMLResponse['description']);
        $rate->setPrice((float) $XMLResponse['price']);

        if (!empty($XMLResponse['fee'])) {
        	$rate->setFee((float) $XMLResponse['fee']);
		} else {
        	$rate->setFee(0);
		}

        $discounts = [];
        $discountFactory = new DiscountFactory();

        if (isset($XMLResponse->discounts) && isset($XMLResponse->discounts->discount)) {
            foreach ($XMLResponse->discounts->discount as $item) {
                $discounts[] = $discountFactory->createDiscount($item);
            }
        }

        $rate->setDiscounts($discounts);

        return $rate;
    }
}