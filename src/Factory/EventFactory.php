<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 11:44
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Event;

class EventFactory
{
    public function createEvent($XMLResponse)
    {
        $event = new Event();
        $event->setEventId((int) $XMLResponse['event_id']);
        $event->setVenueId((int) $XMLResponse['venue_id']);
        $event->setLayoutId((int) $XMLResponse['layout_id']);
        $event->setSoldOut((string) $XMLResponse['sold_out']);
        $event->setSellFrom((string) $XMLResponse['sell_from']);
        $event->setSellTo((string) $XMLResponse['sell_to']);
        $event->setEventDate((string) $XMLResponse['event_date']);
        $event->setConfirmedDate((string) $XMLResponse['confirmed_date']);
        $event->setLastUpdated((string) $XMLResponse['last_updated']);
        $event->setStatus((string) $XMLResponse['status']);
        $event->setExternalMap((string) $XMLResponse['external_map']);

        $zones = [];
        $zoneFactory = new ZoneFactory();
        if (isset($XMLResponse->zone)) {
            foreach ($XMLResponse->zone as $item) {
                $zones[] = $zoneFactory->createZone($item);
            }
        }

        $event->setZones($zones);

        return $event;
    }
}