<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 11:43
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Zone;

class ZoneFactory
{

    public function createZone($XMLResponse)
    {
        $zone = new Zone();
        $zone->setZoneId((int) $XMLResponse['zone_id']);
        $zone->setAreaId((int) $XMLResponse['area_id']);
        $zone->setName((string) $XMLResponse['name']);
        $zone->setCapacity((int) $XMLResponse['capacity']);
        $zone->setMaxPrice((float) $XMLResponse['max_priece']);
        $zone->setNumbered((string) $XMLResponse['numbered']);
        $zone->setAdjacentSeats((int) $XMLResponse['adjacent_seats']);
        $zone->setMaxAdjacentSeats((int) $XMLResponse['max_adjacents_seats']);
        $zone->setAvailableSeats((int) $XMLResponse['available_seats']);

        $seats = [];
        $seatFactory = new SeatFactory();

        if (isset($XMLResponse->seat)) {
            foreach ($XMLResponse->seat as $item) {
                $seats[] = $seatFactory->createSeat($item);
            }
        }

        $zone->setSeats($seats);

        return $zone;
    }
}