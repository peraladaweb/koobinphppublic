<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 11:53
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Seat;

class SeatFactory
{
    public function createSeat($XMLResponse)
    {
        $seat = new Seat();
        $seat->setRowId((int) $XMLResponse['row_id']);
        $seat->setColumnId((int) $XMLResponse['column_id']);
        $seat->setRateId((int) $XMLResponse['rate_id']);
        $seat->setPrice((float) $XMLResponse['price']);
        $seat->setTotal((float) $XMLResponse['total']);
        $seat->setDiscountId((int) $XMLResponse['discount_id']);
        $seat->setDiscountAmount((float) $XMLResponse['discount_amount']);
        $seat->setBarcode((string) $XMLResponse['barcode']);
        $seat->setFee((float) $XMLResponse['fee']);

        return $seat;
    }
}