<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 13:02
 */

namespace Peralada\Koobin\Factory;


use Peralada\Koobin\Util\Discount;

class DiscountFactory
{
    public function createDiscount($XMLResponse)
    {
        $discount = new Discount();
        $discount->setDiscountId((int) $XMLResponse['discount_id']);
        $discount->setDescription((string) $XMLResponse['description']);
        $discount->setDiscount((float) $XMLResponse['discount']);
        $discount->setPercentage((float) $XMLResponse['percentage']);
        $discount->setFee((float) $XMLResponse['fee']);
        $discount->setValidFrom((string) $XMLResponse['valid_from']);
        $discount->setValidTo((string) $XMLResponse['valid_to']);

        return $discount;
    }
}