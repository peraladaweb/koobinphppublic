<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:32
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\BaseEventFactory;
use Peralada\Koobin\Response\EventListResponse;

class EventListRequest extends AbstractBaseRequest
{
    /**
     * @var string
     */
    protected $updated_since;

    /**
     * @var integer
     */
    protected $base_event_id;

    /**
     * @var integer
     */
    protected $event_id;

    public function __construct(GateWay $gateway)
    {
        parent::__construct($gateway);
    }

    /**
     * @return string
     */
    public function getUpdatedSince()
    {
        return $this->updated_since;
    }

    /**
     * @param string $updated_since
     */
    public function setUpdatedSince($updated_since)
    {
        $this->updated_since = $updated_since;
    }

    /**
     * @return int
     */
    public function getBaseEventId()
    {
        return $this->base_event_id;
    }

    /**
     * @param int $base_event_id
     */
    public function setBaseEventId($base_event_id)
    {
        $this->base_event_id = $base_event_id;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param int $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        if (!empty($this->updated_since)) {
            $input->addAttribute('updated_since', $this->updated_since);
        }
        if (!empty($this->base_event_id)) {
            $input->addAttribute('base_event_id', $this->base_event_id);
        }
        if (!empty($this->event_id)) {
            $input->addAttribute('event_id', $this->event_id);
        }

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new EventListResponse();
        $baseEventFactory = new BaseEventFactory();
        $baseEvents = [];
        foreach ($XMLResponse->output->base_event as $baseEvent) {
            $baseEvents[] = $baseEventFactory->createBaseEvent($baseEvent);
        }

        $response->setBaseEvents($baseEvents);

        return $response;
    }
}