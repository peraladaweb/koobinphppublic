<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:32
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\AreaFactory;
use Peralada\Koobin\Response\EventPricesListResponse;

class EventPricesListRequest extends AbstractBaseRequest
{
    /**
     * @var integer
     */
    protected $event_id;

    /**
     * @var string
     */
    protected $last_updated;

    public function __construct(GateWay $gateway)
    {
        parent::__construct($gateway);
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param int $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @return string
     */
    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    /**
     * @param string $last_updated
     */
    public function setLastUpdated($last_updated)
    {
        $this->last_updated = $last_updated;
    }

    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        $input->addAttribute('event_id', $this->event_id);
        if (!empty($this->last_updated)) {
            $input->addAttribute('last_updated', $this->last_updated);
        }

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new EventPricesListResponse();

        $areas = [];
        $areaFactory = new AreaFactory();

        foreach ($XMLResponse->output->area as $item) {
            $areas[] = $areaFactory->createArea($item);
        }

        $response->setAreas($areas);

        return $response;
    }
}