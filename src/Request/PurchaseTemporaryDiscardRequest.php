<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 24/01/2019
 * Time: 17:23
 */

namespace Peralada\Koobin\Request;


class PurchaseTemporaryDiscardRequest extends AbstractBaseRequest
{
	/**
	 * @var integer
	 */
	protected $purchase_id;

	public function __construct(GateWay $gateway)
	{
		parent::__construct($gateway);
	}

	/**
	 * @return int
	 */
	public function getPurchaseId()
	{
		return $this->purchase_id;
	}

	/**
	 * @param int $purchase_id
	 */
	public function setPurchaseId($purchase_id)
	{
		$this->purchase_id = $purchase_id;
	}

	/**
	 * @return Input
	 */
	protected function createXMLParameters()
	{
		$input = new Input();

		$input->addAttribute('purchase_id', $this->purchase_id);

		return $input;
	}

	public function getResponse()
	{
		$XMLResponse = $this->request();

		return (string) $XMLResponse->output['result'];
	}
}