<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 17:08
 */

namespace Peralada\Koobin\Request;


use Exception;
use Peralada\Koobin\Messages\ErrorResponseMessages;

Abstract class AbstractBaseRequest
{
    /**
     * @var GateWay
     */
    private $gate_way;
    private $xmlRequest;
    private $xmlResponse;

    public function __construct(GateWay $gateWay)
    {
        $this->gate_way = $gateWay;
    }

    /**
     * @return mixed
     */
    public function getXmlRequest()
    {
        return $this->xmlRequest;
    }

    /**
     * @param mixed $xmlRequest
     */
    public function setXmlRequest($xmlRequest)
    {
        $this->xmlRequest = $xmlRequest;
    }

    /**
     * @return mixed
     */
    public function getXmlResponse()
    {
        return $this->xmlResponse;
    }

    /**
     * @param mixed $xmlResponse
     */
    public function setXmlResponse($xmlResponse)
    {
        $this->xmlResponse = $xmlResponse;
    }


    /**
     * @return \SimpleXMLElement
     * @throws Exception
     */
    public function request()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->gate_way->getUrl());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->createXMLRequest());
        $this->xmlResponse = curl_exec($ch);

        $response = simplexml_load_string($this->xmlResponse);

        if (isset($response->error)) {
            throw new Exception(ErrorResponseMessages::getMessage((string) $response->error['error_id']), (int) $response->error['error_id']);
        }

        return $response;
    }

    /**
     * @return string
     */
    protected function createXMLRequest()
    {
        $functionName = $this->getFunctionName();

        $this->xmlRequest = '<?xml version="1.0"?>'
            . '<' . $functionName . ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            . 'xsi:noNamespaceSchemaLocation="' . $functionName . '.xsd" version="1.0">'
            . '<user auth="' . $this->gate_way->getAuth() . '" />';
        $this->xmlRequest .= $this->createXMLParameters()->toXML();
        $this->xmlRequest .= '</' . $functionName . '>';

        return $this->xmlRequest;
    }

    protected function createFunctionName($className)
    {
        $names = explode('\\', $className);
        $lastName = end($names);
        return lcfirst(str_replace('Request', '', $lastName));
    }

    /**
     * @return Input
     */
    abstract protected function createXMLParameters();

    /**
     * @return string
     */
    protected function getFunctionName()
    {
        return $this->createFunctionName(static::class);
    }

    abstract public function getResponse();
}