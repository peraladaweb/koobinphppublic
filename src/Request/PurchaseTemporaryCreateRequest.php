<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:31
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\EventFactory;
use Peralada\Koobin\Response\PurchaseResponse;
use Peralada\Koobin\Util\DesiredEvent;

class PurchaseTemporaryCreateRequest extends AbstractBaseRequest
{
    /**
     * @var integer
     */
    protected $purchase_id;

    /**
     * @var DesiredEvent
     */
    protected $desired_event;

    public function __construct(GateWay $gateway)
    {
        parent::__construct($gateway);
    }

    /**
     * @return int
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param int $purchase_id
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;
    }

    /**
     * @return DesiredEvent
     */
    public function getDesiredEvent()
    {
        return $this->desired_event;
    }

    /**
     * @param DesiredEvent $desired_event
     */
    public function setDesiredEvent($desired_event)
    {
        $this->desired_event = $desired_event;
    }

    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        if (!empty($this->purchase_id)) {
            $input->addAttribute('purchase_id', $this->purchase_id);
        }
        if (!empty($this->desired_event)) {
            $input->addContent($this->desired_event);
        }

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new PurchaseResponse();
        $response->setPurchaseId((int) $XMLResponse->output['purchase_id']);
        $response->setTotalAmount((float) $XMLResponse->output['total_amount']);

        $events = [];
        $eventFactory = new EventFactory();
        foreach ($XMLResponse->output->item->event as $item) {
            $events[] = $eventFactory->createEvent($item);
        }

        $response->setEvents($events);

        return $response;
    }
}