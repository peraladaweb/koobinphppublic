<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 13:24
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\ZoneFactory;
use Peralada\Koobin\Response\EventAvailabilityResponse;

class EventAvailabilityRequest extends AbstractBaseRequest
{
    /**
     * @var integer
     */
    protected $event_id;

    /**
     * @var string
     */
    protected $last_updated;

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param int $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @return string
     */
    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    /**
     * @param string $last_updated
     */
    public function setLastUpdated($last_updated)
    {
        $this->last_updated = $last_updated;
    }


    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        $input->addAttribute('event_id', $this->event_id);
        if (!empty($this->last_updated)) {
            $input->addAttribute('last_updated', $this->last_updated);
        }

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new EventAvailabilityResponse();

        $zones = [];
        $zoneFactory = new ZoneFactory();

        foreach ($XMLResponse->output->zone as $item) {
            $zones[] = $zoneFactory->createZone($item);
        }

        $response->setZones($zones);

        return $response;
    }
}