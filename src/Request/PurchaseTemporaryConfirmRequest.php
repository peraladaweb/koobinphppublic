<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:32
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\EventFactory;
use Peralada\Koobin\Response\PurchaseResponse;
use Peralada\Koobin\Util\Customer;

class PurchaseTemporaryConfirmRequest extends AbstractBaseRequest
{
    /**
     * @var integer
     */
    protected $purchase_id;

    /**
     * @var float
     */
    protected $total_amount;

    /**
     * @var Customer
     */
    protected $customer;

    public function __construct(GateWay $gateway)
    {
        parent::__construct($gateway);
    }

    /**
     * @return int
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param int $purchase_id
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * @param float $total_amount
     */
    public function setTotalAmount($total_amount)
    {
        $this->total_amount = $total_amount;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        $input->addAttribute('purchase_id', $this->purchase_id);
        $input->addAttribute('total_amount', $this->total_amount);
        if (!empty($this->customer)) {
            $input->addContent($this->customer);
        }

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new PurchaseResponse();
        $response->setPurchaseId((integer) $XMLResponse->output['purchase_id']);
        $response->setTotalAmount((float) $XMLResponse->output['total_amount']);
        $response->setCustomerReference((string) $XMLResponse->output['customer_reference']);

        $events = [];
        $eventFactory = new EventFactory();
        foreach ($XMLResponse->output->item->event as $item) {
            $events[] = $eventFactory->createEvent($item);
        }

        $response->setEvents($events);

        return $response;
    }
}