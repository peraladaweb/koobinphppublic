<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 05/02/2018
 * Time: 9:48
 */

namespace Peralada\Koobin\Request;


interface InterfaceRequestParameter
{
    public function toXML();
}