<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 01/02/2018
 * Time: 8:54
 */

namespace Peralada\Koobin\Request;


class GateWay
{
    /**
     * @var string
     */
    protected $auth;

    /**
     * @var string
     */
    protected $url;

    /**
     * @return string
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param string $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}