<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 01/02/2018
 * Time: 12:55
 */

namespace Peralada\Koobin\Request;


class Input implements InterfaceRequestParameter
{
    /**
     * @var array
     */
    protected $attributes;
    /**
     * @var array
     */
    protected $body;

    public function __construct()
    {
        $this->attributes = [];
        $this->body = [];
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param array $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }


    public function addContent($item)
    {
        $this->body[] = $item;
    }

    /**
     * @return string
     */
    public function toXML()
    {
        $xml = '<input ';
        if (!empty($this->attributes)) {
            foreach ($this->attributes as $name => $value) {
                $xml .= $name . '="' . $value . '" ';
            }
        }
        $xml .= '>';

        if (!empty($this->body)) {
            foreach ($this->body as $item) {
                $xml .= $item->toXML();
            }
        }

        $xml .= '</input>';

        return $xml;
    }
}