<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:30
 */

namespace Peralada\Koobin\Request;


use Peralada\Koobin\Factory\EventFactory;
use Peralada\Koobin\Response\PurchaseResponse;

class PurchaseTemporaryInfoRequest extends AbstractBaseRequest
{
    protected $purchase_id;

    public function __construct(GateWay $gateway)
    {
        parent::__construct($gateway);
    }

    /**
     * @return mixed
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param mixed $purchase_id
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;
    }

    /**
     * @return Input
     */
    protected function createXMLParameters()
    {
        $input = new Input();

        $input->addAttribute('purchase_id', $this->purchase_id);

        return $input;
    }

    public function getResponse()
    {
        $XMLResponse = $this->request();
        $response = new PurchaseResponse();
        $response->setPurchaseId((int) $XMLResponse->output['purchase_id']);
        $response->setTotalAmount((float) $XMLResponse->output['total_amount']);

        $events = [];
        $eventFactory = new EventFactory();
        foreach ($XMLResponse->output->item->event as $item) {
            $events[] = $eventFactory->createEvent($item);
        }

        $response->setEvents($events);

        return $response;
    }
}