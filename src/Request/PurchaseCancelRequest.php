<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 24/01/2019
 * Time: 11:53
 */

namespace Peralada\Koobin\Request;


class PurchaseCancelRequest extends AbstractBaseRequest
{
	/**
	 * @var integer
	 */
	protected $purchase_id;

	/**
	 * @var float
	 */
	protected $cancelled_amount;

	public function __construct(GateWay $gateway)
	{
		parent::__construct($gateway);
	}

	/**
	 * @return int
	 */
	public function getPurchaseId()
	{
		return $this->purchase_id;
	}

	/**
	 * @param int $purchase_id
	 */
	public function setPurchaseId($purchase_id)
	{
		$this->purchase_id = $purchase_id;
	}

	/**
	 * @return float
	 */
	public function getCancelledAmount()
	{
		return $this->cancelled_amount;
	}

	/**
	 * @param float $cancelled_amount
	 */
	public function setCancelledAmount($cancelled_amount)
	{
		$this->cancelled_amount = $cancelled_amount;
	}

	/**
	 * @return Input
	 */
	protected function createXMLParameters()
	{
		$input = new Input();

		$input->addAttribute('purchase_id', $this->purchase_id);
		$input->addAttribute('cancelled_amount', $this->cancelled_amount);

		return $input;
	}

	public function getResponse()
	{
		$XMLResponse = $this->request();

		return (string) $XMLResponse->output['result'];
	}
}