<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:46
 */

namespace Peralada\Koobin\Response;


class PurchaseResponse
{
    protected $purchase_id;
    protected $customer_reference;
    protected $total_amount;

    /**
     * listado de eventos
     * @var array
     */
    protected $events;

    /**
     * @return mixed
     */
    public function getCustomerReference()
    {
        return $this->customer_reference;
    }

    /**
     * @param mixed $customer_reference
     */
    public function setCustomerReference($customer_reference)
    {
        $this->customer_reference = $customer_reference;
    }

    /**
     * @return mixed
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param mixed $purchase_id
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * @param mixed $total_amount
     */
    public function setTotalAmount($total_amount)
    {
        $this->total_amount = $total_amount;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param array $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }

}