<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 12/02/2018
 * Time: 13:29
 */

namespace Peralada\Koobin\Response;


class EventAvailabilityResponse
{
    /**
     * @var array
     */
    protected $zones;

    /**
     * @return array
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @param array $zones
     */
    public function setZones($zones)
    {
        $this->zones = $zones;
    }
}