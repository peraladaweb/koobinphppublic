<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:37
 */

namespace Peralada\Koobin\Response;


class EventPricesListResponse
{
    /**
     * List of Area
     * @var array
     */
    protected $areas;

    /**
     * @return array
     */
    public function getAreas()
    {
        return $this->areas;
    }

    /**
     * @param array $areas
     */
    public function setAreas($areas)
    {
        $this->areas = $areas;
    }

}