<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:37
 */

namespace Peralada\Koobin\Response;


class EventListResponse
{
    /**
     * @var array
     */
    protected $base_events;

    /**
     * @return array
     */
    public function getBaseEvents()
    {
        return $this->base_events;
    }

    /**
     * @param array $base_events
     */
    public function setBaseEvents($base_events)
    {
        $this->base_events = $base_events;
    }

}