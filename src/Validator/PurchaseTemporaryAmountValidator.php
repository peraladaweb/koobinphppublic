<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 14/02/2019
 * Time: 10:09
 */

namespace Peralada\Koobin\Validator;


use Peralada\Koobin\Request\EventPricesListRequest;
use Peralada\Koobin\Request\GateWay;
use Peralada\Koobin\Request\PurchaseTemporaryInfoRequest;
use Peralada\Koobin\Response\PurchaseResponse;

class PurchaseTemporaryAmountValidator
{
	protected $gateway;
	protected $purchase_id;

	public function __construct(GateWay $gateway, $purchase_id)
	{
		$this->gateway = $gateway;
		$this->$purchase_id = $purchase_id;
	}

	public function validate()
	{
		$purchaseTemporaryResponse = $this->getPurchaseTemporaryResponse();
		$eventsIds = $this->getEventsIds($purchaseTemporaryResponse);
		$eventsPricesListsResponses = $this->getEventsPricesListResponses($eventsIds);

		// verificar que los códigos descuento de cada asiento coinciden con los códigos que pueden usarse en ese evento
		// verificar limites de los códigos descuento
		// verificar limites de la tarifa
		// verificar fees

		// calcular el amount total

		// ha de devolver un status y el precio total de la compra
	}

	protected function getPurchaseTemporaryResponse()
	{
		$purchaseTemporaryInfoRequest = new PurchaseTemporaryInfoRequest($this->gateway);
		$purchaseTemporaryInfoRequest->setPurchaseId($this->purchase_id);

		return $purchaseTemporaryInfoRequest->getResponse();
	}

	protected function getEventsIds(PurchaseResponse $purchaseResponse)
	{
		$events = $purchaseResponse->getEvents();

		$eventsIds = [];
		foreach ($events as $event) {
			$eventsIds[] = $event->getEventId();
		}

		return $eventsIds;
	}

	protected function getEventsPricesListResponses(array $eventsIds)
	{
		$eventsPricesListsResponses = [];
		foreach ($eventsIds as $eventId) {
			$eventPricesListRequest = new EventPricesListRequest($this->gateway);
			$eventPricesListRequest->setEventId($eventId);

			$eventsPricesListsResponses[] = $eventPricesListRequest->getResponse();
		}

		return $eventsPricesListsResponses;
	}
}