<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 27/03/2018
 * Time: 12:38
 */

namespace Peralada\Koobin\Messages;


class ErrorResponseMessages
{
    static function getMessages() {
        return [
            '-1' => 'No existe la combinación aforo/configuración',
            '-2' => 'No se ha especificado la cantidad de entradas',
            '-3' => 'La tarifa no es válida',
            '-4' => 'El descuento no es válido',
            '-5' => 'No hay suficientes localidades',
            '-6' => 'La localidad no está disponible',
            '-7' => 'No existe ese temporal o está vacío',
            '-8' => 'No existe el cliente',
            '-9' => 'El importe enviado no coincide con el esperado',
            '-10' => 'No existe Localidad',
            '-11' => 'No tiene permiso para anular esta entrada',
            '-12' => 'Ese evento ya ha pasado',
            '-13' => 'La entrada ya ha sido utilizada',
            '-14' => 'No se ha podido anular la entrada',
            '-15' => 'No existe esa configuración',
            '-16' => 'No existe la sesión o no tiene permisos para vela',
            '-17' => 'El formato de la fecha no es correcto',
            '-18' => 'No existe la zona o no tiene permisos',
            '-19' => 'No existe ese evento o no tiene permisos para verlo',
            '-20' => 'El evento está cancelado',
            '-21' => 'No tiene permisos para imprimir esa compra',
            '-22' => 'Este cliente no tiene activo el formato móvil',
            '-23' => 'No existe la empresa o no está asignada a ningún evento',
            '-24' => 'No existe la compra temporal especificada o no tiene permiso para modificarla',
            '-25' => 'Alguna de las tarifas ha superado el máximo global',
            '-26' => 'Alguna de las tarifas ha superado el máximo de localidades permitidas por evento',
            '-27' => 'Alguna de las tarifas ha superado el máximo de localidades permitidas en una misma compra',
            '-28' => 'Alguna de las tarifas no ha llegado al mínimo de localidades por evento',
            '-29' => 'Alguna de las tarifas no ha llegado al mínimo de localidades por compra',
            '-30' => 'Alguno de los descuentos ha superado el máximo de localidades por compra',
            '-31' => 'Alguno de los descuentos ha superado el máximo de localidades por evento',
            '-32' => 'Alguno de los descuentos no ha llegado al mínimo de localidades por evento',
            '-33' => 'El evento que se está intentando comprar no está a la venta en estos momentos',
            '-34' => 'Esa configuración no tiene mapeo disponible',
            '-90' => 'Hay que volver a actualizar la información del evento',
            '-99' => 'Error al asignar las localidades',
            '-100' => 'Versión mensajería incorrecta',
            '-110' => 'El XML no está bien formado',
            '-999' => 'Ha fallado la autenticación'
        ];
    }

    /**
     * @param string $key
     * @return string
     */
    static function getMessage($key)
    {
        if (!empty(self::getMessages()[$key])) {
            return self::getMessages()[$key];
        } else {
            return 'Error con la conexión con Koobin';
        }
    }
}