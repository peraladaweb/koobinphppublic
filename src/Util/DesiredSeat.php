<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 05/02/2018
 * Time: 9:44
 */

namespace Peralada\Koobin\Util;


use Peralada\Koobin\Request\InterfaceRequestParameter;

class DesiredSeat implements InterfaceRequestParameter
{
    /**
     * @var integer
     */
    protected $rate_id;

    /**
     * @var integer
     */
    protected $discount_id;

    /**
     * @var integer
     */
    protected $row_id;

    /**
     * @var integer
     */
    protected $column_id;

    /**
     * @var  integer
     */
    protected $quantity;

    /**
     * @return int
     */
    public function getRateId()
    {
        return $this->rate_id;
    }

    /**
     * @param int $rate_id
     */
    public function setRateId($rate_id)
    {
        $this->rate_id = $rate_id;
    }

    /**
     * @return int
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     * @param int $discount_id
     */
    public function setDiscountId($discount_id)
    {
        $this->discount_id = $discount_id;
    }

    /**
     * @return int
     */
    public function getRowId()
    {
        return $this->row_id;
    }

    /**
     * @param int $row_id
     */
    public function setRowId($row_id)
    {
        $this->row_id = $row_id;
    }

    /**
     * @return int
     */
    public function getColumnId()
    {
        return $this->column_id;
    }

    /**
     * @param int $column_id
     */
    public function setColumnId($column_id)
    {
        $this->column_id = $column_id;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    public function toXML()
    {
        $xml = '<desired_seat ' .
            'rate_id="' . $this->rate_id . '" ';

        if (!empty($this->discount_id)) {
            $xml .= 'discount_id="' . $this->discount_id . '" ';
        }

        if (!empty($this->row_id)) {
            $xml .= 'discount_id="' . $this->row_id . '" ';
        }

        if (!empty($this->column_id)) {
            $xml .= 'discount_id="' . $this->column_id . '" ';
        }

        if (!empty($this->quantity)) {
            $xml .= 'quantity="' . $this->quantity . '" ';
        }

        $xml .= '>';
        $xml .= '</desired_seat>';

        return $xml;
    }
}