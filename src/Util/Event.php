<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:52
 */

namespace Peralada\Koobin\Util;


class Event
{
    /**
     * @var integer
     */
    protected $event_id;

    /**
     * @var integer
     */
    protected $venue_id;

    /**
     * @var integer
     */
    protected $layout_id;

    /**
     * @var boolean
     */
    protected $sold_out;

    /**
     * @var string
     */
    protected $sell_from;

    /**
     * @var string
     */
    protected $sell_to;

    /**
     * @var string
     */
    protected $event_date;

    /**
     * @var true
     */
    protected $confirmed_date;

    /**
     * @var string
     */
    protected $last_updated;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var boolean
     */
    protected $external_map;

    /**
     * @return mixed
     */
    public function getVenueId()
    {
        return $this->venue_id;
    }

    /**
     * @param mixed $venue_id
     */
    public function setVenueId($venue_id)
    {
        $this->venue_id = $venue_id;
    }

    /**
     * @return mixed
     */
    public function getLayoutId()
    {
        return $this->layout_id;
    }

    /**
     * @param mixed $layout_id
     */
    public function setLayoutId($layout_id)
    {
        $this->layout_id = $layout_id;
    }

    /**
     * @return mixed
     */
    public function getSoldOut()
    {
        return $this->sold_out;
    }

    /**
     * @param mixed $sold_out
     */
    public function setSoldOut($sold_out)
    {
        $this->sold_out = $sold_out;
    }

    /**
     * @return mixed
     */
    public function getSellFrom()
    {
        return $this->sell_from;
    }

    /**
     * @param mixed $sell_from
     */
    public function setSellFrom($sell_from)
    {
        $this->sell_from = $sell_from;
    }

    /**
     * @return mixed
     */
    public function getSellTo()
    {
        return $this->sell_to;
    }

    /**
     * @param mixed $sell_to
     */
    public function setSellTo($sell_to)
    {
        $this->sell_to = $sell_to;
    }

    /**
     * @return mixed
     */
    public function getEventDate()
    {
        return $this->event_date;
    }

    /**
     * @param mixed $event_date
     */
    public function setEventDate($event_date)
    {
        $this->event_date = $event_date;
    }

    /**
     * @return mixed
     */
    public function getConfirmedDate()
    {
        return $this->confirmed_date;
    }

    /**
     * @param mixed $confirmed_date
     */
    public function setConfirmedDate($confirmed_date)
    {
        $this->confirmed_date = $confirmed_date;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    /**
     * @param mixed $last_update
     */
    public function setLastUpdated($last_update)
    {
        $this->last_updated = $last_update;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getExternalMap()
    {
        return $this->external_map;
    }

    /**
     * @param mixed $external_map
     */
    public function setExternalMap($external_map)
    {
        $this->external_map = $external_map;
    }
    /**
     * @var array
     */
    protected $zones;

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param mixed $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @return array
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @param array $zones
     */
    public function setZones($zones)
    {
        $this->zones = $zones;
    }

    /**
     * @param Zone $zone
     */
    public function addZone($zone)
    {
        $this->zones[] = $zone;
    }
}