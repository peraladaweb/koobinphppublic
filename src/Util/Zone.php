<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:53
 */

namespace Peralada\Koobin\Util;


class Zone
{
    /**
     * @var integer
     */
    protected $zone_id;
    /**
     * @var integer
     */
    protected $area_id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var integer
     */
    protected $capacity;
    /**
     * @var float
     */
    protected $max_price;
    /**
     * @var boolean
     */
    protected $numbered;
    /**
     * @var integer
     */
    protected $adjacent_seats;
    /**
     * list of Seat
     * @var array
     */
    protected $seats;

    /**
     * @var integer
     */
    protected $max_adjacent_seats;

    /**
     * @var integer
     */
    protected $available_seats;

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getAdjacentSeats()
    {
        return $this->adjacent_seats;
    }

    /**
     * @param mixed $adjacent_seats
     */
    public function setAdjacentSeats($adjacent_seats)
    {
        $this->adjacent_seats = $adjacent_seats;
    }

    /**
     * @return array
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param array $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * @param int $area_id
     */
    public function setAreaId($area_id)
    {
        $this->area_id = $area_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return float
     */
    public function getMaxPrice()
    {
        return $this->max_price;
    }

    /**
     * @param float $max_price
     */
    public function setMaxPrice($max_price)
    {
        $this->max_price = $max_price;
    }

    /**
     * @return bool
     */
    public function isNumbered()
    {
        return $this->numbered;
    }

    /**
     * @param bool $numbered
     */
    public function setNumbered($numbered)
    {
        $this->numbered = $numbered;
    }

    /**
     * @return int
     */
    public function getMaxAdjacentSeats()
    {
        return $this->max_adjacent_seats;
    }

    /**
     * @param int $max_adjacent_seats
     */
    public function setMaxAdjacentSeats($max_adjacent_seats)
    {
        $this->max_adjacent_seats = $max_adjacent_seats;
    }

    /**
     * @return int
     */
    public function getAvailableSeats()
    {
        return $this->available_seats;
    }

    /**
     * @param int $available_seats
     */
    public function setAvailableSeats($available_seats)
    {
        $this->available_seats = $available_seats;
    }

}