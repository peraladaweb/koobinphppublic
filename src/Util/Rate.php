<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 15:30
 */

namespace Peralada\Koobin\Util;


class Rate
{
    /**
     * @var integer
     */
    protected $rate_id;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var float
     */
    protected $price;
    /**
     * List of Discount
     * @var array
     */
    protected $discounts;

	/**
	 * @var float
	 */
    protected $fee;

    /**
     * @return int
     */
    public function getRateId()
    {
        return $this->rate_id;
    }

    /**
     * @param int $rate_id
     */
    public function setRateId($rate_id)
    {
        $this->rate_id = $rate_id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return array
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @param array $discounts
     */
    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;
    }

	/**
	 * @return float
	 */
	public function getFee()
	{
		return $this->fee;
	}

	/**
	 * @param float $fee
	 */
	public function setFee($fee)
	{
		$this->fee = $fee;
	}

}