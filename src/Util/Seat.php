<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 13:53
 */

namespace Peralada\Koobin\Util;


class Seat
{
    protected $row_id;
    protected $column_id;
    protected $rate_id;
    protected $discount_id;
    protected $barcode;
    protected $simplified_invoice_number;
    protected $price;
    protected $total;
    protected $fee;
    protected $visibility_pct;
    protected $discount_amount;

    /**
     * @return mixed
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param mixed $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return mixed
     */
    public function getRowId()
    {
        return $this->row_id;
    }

    /**
     * @param mixed $row_id
     */
    public function setRowId($row_id)
    {
        $this->row_id = $row_id;
    }

    /**
     * @return mixed
     */
    public function getColumnId()
    {
        return $this->column_id;
    }

    /**
     * @param mixed $column_id
     */
    public function setColumnId($column_id)
    {
        $this->column_id = $column_id;
    }

    /**
     * @return mixed
     */
    public function getRateId()
    {
        return $this->rate_id;
    }

    /**
     * @param mixed $rate_id
     */
    public function setRateId($rate_id)
    {
        $this->rate_id = $rate_id;
    }

    /**
     * @return mixed
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     * @param mixed $discount_id
     */
    public function setDiscountId($discount_id)
    {
        $this->discount_id = $discount_id;
    }

    /**
     * @return mixed
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param mixed $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return mixed
     */
    public function getSimplifiedInvoiceNumber()
    {
        return $this->simplified_invoice_number;
    }

    /**
     * @param mixed $simplified_invoice_number
     */
    public function setSimplifiedInvoiceNumber($simplified_invoice_number)
    {
        $this->simplified_invoice_number = $simplified_invoice_number;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param mixed $fee
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    }

    /**
     * @return mixed
     */
    public function getVisibilityPct()
    {
        return $this->visibility_pct;
    }

    /**
     * @param mixed $visibility_pct
     */
    public function setVisibilityPct($visibility_pct)
    {
        $this->visibility_pct = $visibility_pct;
    }

}