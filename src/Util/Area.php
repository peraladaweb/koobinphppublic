<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 15:29
 */

namespace Peralada\Koobin\Util;


class Area
{
    /**
     * @var integer
     */
    protected $area_id;
    /**
     * @var string
     */
    protected $description;
    /**
     * List of Rate
     * @var array
     */
    protected $rates;

    /**
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * @param int $area_id
     */
    public function setAreaId($area_id)
    {
        $this->area_id = $area_id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param array $rates
     */
    public function setRates($rates)
    {
        $this->rates = $rates;
    }

}