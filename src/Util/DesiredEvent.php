<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 05/02/2018
 * Time: 9:44
 */

namespace Peralada\Koobin\Util;


use Peralada\Koobin\Request\InterfaceRequestParameter;

class DesiredEvent implements InterfaceRequestParameter
{
    /**
     * @var integer
     */
    protected $event_id;

    /**
     * @var string
     */
    protected $last_update;

    /**
     * @var array
     */
    protected $desired_zones;

    public function __construct()
    {
        $this->desired_zones = [];
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param int $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @return string
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * @param string $last_update
     */
    public function setLastUpdate($last_update)
    {
        $this->last_update = $last_update;
    }

    /**
     * @return array
     */
    public function getDesiredZones()
    {
        return $this->desired_zones;
    }

    /**
     * @param array $desired_zones
     */
    public function setDesiredZones($desired_zones)
    {
        $this->desired_zones = $desired_zones;
    }

    /**
     * @param DesiredZone $desired_zone
     */
    public function addDesiredZone($desired_zone)
    {
        $this->desired_zones[] = $desired_zone;
    }

    public function toXML()
    {
        $xml = '<desired_event ' .
            'event_id="' . $this->event_id . '" ';
        if (!empty($this->last_update)) {
            $xml .= 'last_updated="' . $this->last_update . '" ';
        }
        $xml .= '>';

        foreach ($this->desired_zones as $zone) {
            $xml .= $zone->toXML();
        }

        $xml .= '</desired_event>';

        return $xml;
    }
}