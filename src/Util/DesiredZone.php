<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 05/02/2018
 * Time: 9:44
 */

namespace Peralada\Koobin\Util;


use Peralada\Koobin\Request\InterfaceRequestParameter;

class DesiredZone implements InterfaceRequestParameter
{
    /**
     * @var integer
     */
    protected $zone_id;

    /**
     * @var array
     */
    protected $desired_seats;

    public function __construct()
    {
        $this->desired_seats = [];
    }

    /**
     * @return int
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param int $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return array
     */
    public function getDesiredSeats()
    {
        return $this->desired_seats;
    }

    /**
     * @param array $desired_seats
     */
    public function setDesiredSeats($desired_seats)
    {
        $this->desired_seats = $desired_seats;
    }

    /**
     * @param array $desired_seat
     */
    public function addDesiredSeat($desired_seat)
    {
        $this->desired_seats[] = $desired_seat;
    }


    public function toXML()
    {
        $xml = '<desired_zone ' .
            'zone_id="' . $this->zone_id . '" >';

        foreach ($this->desired_seats as $seat) {
            $xml .= $seat->toXML();
        }

        $xml .= '</desired_zone>';

        return $xml;
    }
}