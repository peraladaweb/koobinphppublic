<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 14:03
 */

namespace Peralada\Koobin\Util;


use Peralada\Koobin\Request\InterfaceRequestParameter;

class Customer implements InterfaceRequestParameter
{
    const DOCUMENT_TYPE_NIF = 'NIF';
    const DOCUMENT_TYPE_NIE = 'NIE';
    const DOCUMENT_TYPE_PASAPORTE = 'PASSPORT';

    protected $name;
    protected $surname_1st;
    protected $surname_2nd;
    protected $email;
    protected $phone;
    protected $mobile;
    protected $language;
    protected $address_country;
    protected $address_zip_code;
    protected $address_street;
    protected $address_number;
    protected $address_staircase;
    protected $address_floor;
    protected $address_door;
    protected $address_city;
    protected $address_province;
    protected $birth_date;
    protected $document_type;
    protected $document_number;
    protected $remarks;

    /**
     * @return mixed
     */
    public function getAddressCountry()
    {
        return $this->address_country;
    }

    /**
     * @param mixed $country
     */
    public function setAddressCountry($country)
    {
        $this->address_country = $country;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birth_date;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthdate($birthday)
    {
        $this->birth_date = $birthday;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname1st()
    {
        return $this->surname_1st;
    }

    /**
     * @param mixed $surname_1st
     */
    public function setSurname1st($surname_1st)
    {
        $this->surname_1st = $surname_1st;
    }

    /**
     * @return mixed
     */
    public function getSurname2nd()
    {
        return $this->surname_2nd;
    }

    /**
     * @param mixed $surname_2st
     */
    public function setSurname2nd($surname_2st)
    {
        $this->surname_2nd = $surname_2st;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }



    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getAddressZipCode()
    {
        return $this->address_zip_code;
    }

    /**
     * @param mixed $adress_zip_code
     */
    public function setAddressZipCode($adress_zip_code)
    {
        $this->address_zip_code = $adress_zip_code;
    }

    /**
     * @return mixed
     */
    public function getDocumentType()
    {
        return $this->document_type;
    }

    /**
     * @param mixed $document_type
     */
    public function setDocumentType($document_type)
    {
        $this->document_type = $document_type;
    }

    /**
     * @return mixed
     */
    public function getDocumentNumber()
    {
        return $this->document_number;
    }

    /**
     * @param mixed $document_number
     */
    public function setDocumentNumber($document_number)
    {
        $this->document_number = $document_number;
    }

    /**
     * @return mixed
     */
    public function getAddressStreet()
    {
        return $this->address_street;
    }

    /**
     * @param mixed $address_street
     */
    public function setAddressStreet($address_street)
    {
        $this->address_street = $address_street;
    }

    /**
     * @return mixed
     */
    public function getAddressNumber()
    {
        return $this->address_number;
    }

    /**
     * @param mixed $address_number
     */
    public function setAddressNumber($address_number)
    {
        $this->address_number = $address_number;
    }

    /**
     * @return mixed
     */
    public function getAddressStaircase()
    {
        return $this->address_staircase;
    }

    /**
     * @param mixed $address_staircase
     */
    public function setAddressStaircase($address_staircase)
    {
        $this->address_staircase = $address_staircase;
    }

    /**
     * @return mixed
     */
    public function getAddressFloor()
    {
        return $this->address_floor;
    }

    /**
     * @param mixed $address_floor
     */
    public function setAddressFloor($address_floor)
    {
        $this->address_floor = $address_floor;
    }

    /**
     * @return mixed
     */
    public function getAddressDoor()
    {
        return $this->address_door;
    }

    /**
     * @param mixed $address_door
     */
    public function setAddressDoor($address_door)
    {
        $this->address_door = $address_door;
    }

    /**
     * @return mixed
     */
    public function getAddressCity()
    {
        return $this->address_city;
    }

    /**
     * @param mixed $address_city
     */
    public function setAddressCity($address_city)
    {
        $this->address_city = $address_city;
    }

    /**
     * @return mixed
     */
    public function getAddressProvince()
    {
        return $this->address_province;
    }

    /**
     * @param mixed $address_province
     */
    public function setAddressProvince($address_province)
    {
        $this->address_province = $address_province;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param mixed $remarks
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }



    public function toXML()
    {
        $xml = '<customer>';

        if (!empty($this->name)) {
            $xml .= '<name>' . $this->name . '</name>';
        }

        if (!empty($this->surname_1st)) {
            $xml .= '<surname_1st>' . $this->surname_1st . '</surname_1st>';
        }

        if (!empty($this->surname_2nd)) {
            $xml .= '<surname_2nd>' . $this->surname_2nd . '</surname_2nd>';
        }

        if (!empty($this->email)) {
            $xml .= '<email>' . $this->email . '</email>';
        }

        if (!empty($this->phone)) {
            $xml .= '<phone>' . $this->phone . '</phone>';
        }

        if (!empty($this->mobile)) {
            $xml .= '<mobile>' . $this->mobile . '</mobile>';
        }

        if (!empty($this->language)) {
            $xml .= '<language>' . $this->language . '</language>';
        }

        if (!empty($this->address_country)) {
            $xml .= '<address_country>' . $this->address_country . '</address_country>';
        }

//        if (!empty($this->birth_date)) {
//            $xml .= '<birth_date>' . $this->birth_date . '</birth_date>';
//        }

        if (!empty($this->address_zip_code)) {
            $xml .= '<address_zip_code>' . $this->address_zip_code . '</address_zip_code>';
        }

        if (!empty($this->address_street)) {
            $xml .= '<address_street>' . $this->address_street . '</address_street>';
        }

        if (!empty($this->address_number)) {
            $xml .= '<address_number>' . $this->address_number . '</address_number>';
        }

        if (!empty($this->address_staircase)) {
            $xml .= '<address_staircase>' . $this->address_staircase . '</address_staircase>';
        }

        if (!empty($this->address_floor)) {
            $xml .= '<address_floor>' . $this->address_floor . '</address_floor>';
        }

        if (!empty($this->address_door)) {
            $xml .= '<address_door>' . $this->address_door . '</address_door>';
        }

        if (!empty($this->address_city)) {
            $xml .= '<address_city>' . $this->address_city . '</address_city>';
        }

        if (!empty($this->address_province)) {
            $xml .= '<address_province>' . $this->address_province . '</address_province>';
        }

        if (!empty($this->document_type)) {
            $xml .= '<document_type>' . $this->document_type . '</document_type>';
        }

        if (!empty($this->document_number)) {
            $xml .= '<document_number>' . $this->document_number . '</document_number>';
        }

        if (!empty($this->remarks)) {
            $xml .= '<remarks>' . $this->remarks . '</remarks>';
        }

        $xml .= '</customer>';

        return $xml;
    }
}