<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 16:33
 */

namespace Peralada\Koobin\Util;


class BaseEvent
{
    /**
     * @var integer
     */
    protected $base_event_id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $organizer_company_vat_number;
    /**
     * @var integer
     */
    protected $organizer_comany_id;
    /**
     * List of Event
     * @var array
     */
    protected $events;

    /**
     * @return int
     */
    public function getBaseEventId()
    {
        return $this->base_event_id;
    }

    /**
     * @param int $base_event_id
     */
    public function setBaseEventId($base_event_id)
    {
        $this->base_event_id = $base_event_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getOrganizerCompanyVatNumber()
    {
        return $this->organizer_company_vat_number;
    }

    /**
     * @param string $organizer_company_vat_number
     */
    public function setOrganizerCompanyVatNumber($organizer_company_vat_number)
    {
        $this->organizer_company_vat_number = $organizer_company_vat_number;
    }

    /**
     * @return int
     */
    public function getOrganizerComanyId()
    {
        return $this->organizer_comany_id;
    }

    /**
     * @param int $organizer_comany_id
     */
    public function setOrganizerComanyId($organizer_comany_id)
    {
        $this->organizer_comany_id = $organizer_comany_id;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param array $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }
}