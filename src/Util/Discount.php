<?php
/**
 * Created by PhpStorm.
 * User: oscar.garcia
 * Date: 30/01/2018
 * Time: 15:32
 */

namespace Peralada\Koobin\Util;


class Discount
{
    /**
     * @var integer
     */
    protected $discount_id;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var float
     */
    protected $discount;
    /**
     * @var boolean
     */
    protected $percentage;

    /**
     * @var float
     */
    protected $fee;

    /**
     * @var string
     */
    protected $valid_from;

    /**
     * @var string
     */
    protected $valid_to;

    /**
     * @return int
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     * @param int $discount_id
     */
    public function setDiscountId($discount_id)
    {
        $this->discount_id = $discount_id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return bool
     */
    public function isPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param bool $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param float $fee
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    }

    /**
     * @return string
     */
    public function getValidFrom()
    {
        return $this->valid_from;
    }

    /**
     * @param string $valid_from
     */
    public function setValidFrom($valid_from)
    {
        $this->valid_from = $valid_from;
    }

    /**
     * @return string
     */
    public function getValidTo()
    {
        return $this->valid_to;
    }

    /**
     * @param string $valid_to
     */
    public function setValidTo($valid_to)
    {
        $this->valid_to = $valid_to;
    }
}