#How to use:
First, you need to create a gateway:

    $auth = 'yourAuthHash';
    $url = 'yourKoobinUrl';
    
    $gateway = new GateWay();
    $gateway->setAuth($auth);
    $gateway->setUrl($url);


Second, make the request:

    $eventListResquest = new EventListRequest($gateWay);
    $eventListResponse = $eventListResquest->getResponse();
    
If the request need parameters:
    
    $eventListResquest = new EventListRequest($gateWay);
    $eventListResquest->setUpdatedSince($updatedSince);
    $eventListResponse = $eventListResquest->getResponse();
    
If you want to work directly with XML responses:

    $eventListResquest = new EventListRequest($gateWay);
    $eventListResquest->setUpdatedSince($updatedSince);
    $XMLResponse = $eventListResquest->request();
    
The getResponse() function, of the requests classes, is the XML response
parsed to object


